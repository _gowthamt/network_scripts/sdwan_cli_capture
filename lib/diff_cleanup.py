#/usr/bin/python3

###############################################################################
#
#
# Copyright 2020, Cisco Systems, Inc.
# All Rights Reserved.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
#
# Diff Cleanup script is integrated with the sdwan_pre_post_diff playbook.
#
# Created by Gowtham Tamilselvan
# Revision Number: 1.2
# Last Updated: Aug 19, 2020
# Cisco Systems, Inc./SDWAN SRE TEAM
#
# This python script produces 2 output files:
#   1: Produces an Output file called - {{filename}}_diff_{{timestamp}}.html
#      This file contains the Full Precehck - Postcheck diff comparisons side by side.
#
#   2: Produces an Output file called - {{filename}}_reduced_diff_{{timestamp}}.html
#      This file contains the reduced line by line diff comparison.
#
#
# There is no need to run this script independently!
#
###############################################################################

import sys
import argparse
from collections import OrderedDict
import re
import difflib

MIN_PYTHON = (3, 1)
if sys.version_info < MIN_PYTHON:
    sys.exit("Python %s.%s or later is required.\n" % MIN_PYTHON)

def readfile(filename):
    """Read a file."""
    #print('Reading file "' + filename + '"')
    try:
        with open(filename, 'r') as filehandler:
            lines = filehandler.readlines()
    except IOError as error:
        print('Unable to open file ' + filename  + 'for reading, exiting.')
        sys.exit()
    return lines

def writefile(filename, data):
    """Write to a file."""
    #print('Writing file "' + filename + '"')
    try:
        with open(filename, 'w') as filewrite:
            filewrite.writelines(data)
    except IOError as error:
        print('Unable to open file ' + filename  + 'for writing, exiting.')
        sys.exit()

def write_as_html_file(filename, data):
    """Write Diff output file as an html file."""
    #htmlfname = (filename + ".html")
    #print(filename)
    try:
        with open(filename, 'w') as htmlfile:
            htmlfile.write("<pre>")
            for line in data:
                if re.search("===", line):
                    htmlfile.write('<span style="background-color:White;font-weight:bold;color:blue">' + line + '</span>')
                elif re.search("^<\s+", line):
                    htmlfile.write('<span style="background-color:LightCoral;font-weight:normal;color:black">' + line + '</span>')
                elif re.search("^>\s+", line):
                    htmlfile.write('<span style="background-color:LightGreen;font-weight:normal;color:black">' + line + '</span>')
                else:
                    htmlfile.write(line)
            htmlfile.write("</pre>")
    except IOError as error:
        print('Unable to open file ' + filename  + 'for writing, exiting.')
        sys.exit()
    #print(htmlfile)
    htmlfile.close()

def diff_processing(data):
    """Remove Duplicates from diffdata file."""
    # Loop through the file and remove any lines that only contain the precheck and postcheck command cli without any actual cli output
    # Compare first line with thrid line and if they are both prechecks or postcheck command line and not outputs then empty the current line (first line)
    for i in range(len(data)):
        if i + 2 < len(data):
            if (data[i].startswith('< === ') & data[i+2].startswith('< ===')) | (data[i].startswith('> === ') & data[i+2].startswith('> ===')):
                data[i] = ''
            else:
                newdiff.append(data[i])
        else:
            break
    # Remove duplicate items from newdiff, to reduce size to just the diff items
    diff_dup_removed = list(OrderedDict.fromkeys(newdiff))
    return diff_dup_removed

##### MAIN #####

parser = argparse.ArgumentParser()
parser.add_argument('-c', action='store_true', default=False,
                    help='Produce a context format diff (default False)')
parser.add_argument('-l', '--lines', type=int, default=3,
                    help='Set number of context lines (default 3)')
parser.add_argument('prefile')
parser.add_argument('postfile')
parser.add_argument('htmloutputfilename')
parser.add_argument('processeddiff')
options = parser.parse_args()

n = options.lines
prefile = options.prefile
postfile = options.postfile
outputfile = options.htmloutputfilename # used to create side by side html file
difffile = options.processeddiff # used to create reduced diff file

### ---- FILE 1 of 2 - FULL HTML SIDE BY SIDE COMPARISON OUTPUT ----
#Produces an Output file called - {{filename}}_diff_{{timestamp.html}}

# READ PRE AND POST FILES
prelines = readfile(prefile)
postlines = readfile(postfile)


# Create the Full Diff file with side by side HTML Comparison
with open(outputfile, 'w') as f:
    f.write(difflib.HtmlDiff(wrapcolumn=80).make_file(prelines, postlines, prefile, postfile, context=options.c, numlines=n))

### ---- FILE 2 of 2 - REDUCING THE DIFF'd FILE TO REMOVE DUPLICATES OUTPUT ----
# Produces an Output file called - {{filename}}_reduced_diff_{{timestamp}}.html

# Further Edit the Processed Diff File that provided as an input file
# Goal is to remove duplicate data in Diff file

# Read Processed Diff File
diffdata = readfile(difffile)

newdiff = []

reduced_data = diff_processing(diffdata)

# Overwrite the initial file with new diff data
#writefile(filename)
write_as_html_file(difffile, reduced_data)
