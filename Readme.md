# Disclaimer
**This function pack may be used internally but in order to deliver it to a customer the appropiate SOW must be in place.**

# **<p align="center">SDWAN CLI CAPTURE SCRIPT</p>**

# Overview

This tool was created to assist in cli data collection from sdwan devices (vcontroller or vedges). 

This tool provides 3 main functions:
- Ad-hoc cli data collection
- Precheck and postcheck data collection (for Maintainence/change windows) 
- Diff comparison between the precheck and postcheck data

The entire data collection (adhoc/pre/post) functionality is handled by the sdwan_cli_captures.yml ansible playbook file.

The difference comparison functionality is handled by the sdwan_pre_post_diff.yml ansible playbook file.

# Contacts
## Author

Gowtham Tamilselvan <gtamilse@cisco.com>

## Support

CX-TTG-SRE Team <group.slaligam@cisco.com>

# Version

1. sdwan_cli_capture.yml script - Version 1.10
2. sdwan_pre_post_diff.yml script - Version 1.8

# Release Date
*Specify the realease date for the version stated above*
August 19 2020

# Use Case(s)

## 1. Data Collection - sdwan_cli_captures.yml

### SDWAN CLI CAPTURE PLAYBOOK

- Cisco Product tested 			: vedge cloud, vmanage, vbond, vsmart
- Cisco IOS XR Software Version tested  : 19.2.099

#### Playbook Requirements:

  * Note: if you clone this git repo, all the required template files will already exist in their appropriate directories, you will only need to create your own inventory file and cli commands file.

   1: Jinja2 template files called sdwan_cli_capture_template.j2, sdwan_cli_precheck_template.j2 sdwan_cli_postcheck_template.j2 are required file.

   2: List of cli commands in txt file (file name does not matter). 
   A default file (sdwan_cli_cmds.txt) is provided in commands directory, but you can create as many new commands file as needed.

 - Recommend to use the "| nomore" option with the CLI commands

```
Example: 
    show system status | nomore
    show interface | nomore
```

   3: Ansible inventory/hosts file defined with hostnames (not just IPs)
```
Example: 
    [edges]
    vEdge1 ansible_host=10.10.10.11 ansible_user=admin ansible_ssh_pass=admin ansible_network_os=ios
```

#### Playbook Summary:

1: Playbook can be used for adhoc data collection, by simiply providing a command file and defining the host
```
Run script in adhoc mode by specifying the following parameters
    --tags collect
    -e "variable_host=<inventory_host/group_name>"
    -e "cmds_file=<cli_cmds_file.txt>" 

- Example: ansible-playbook sdwan_cli_capture.yml --tags collect -e "variable_host=vEdge1" -e "cmds_file=sdwan_cli_cmds.txt"

- All output logs are stored in the /tmp/ansible_logs/ directory on the local host

```

2: Playbook can be used for Pre and Post Capture Analysis during Maintenance windows 
- PRECHECK OPTION
```
Run the precheck option before the window starts by specifying the following parameters
    --tags precheck
    -e "variable_host=<inventory_host/group_name>"
    -e "cmds_file=<cli_cmds_file.txt>" 

- Example: ansible-playbook sdwan_cli_capture.yml --tags precheck -e "variable_host=vEdge1" -e "cmds_file=sdwan_cli_cmds.txt"

- All output logs are stored in the /tmp/ansible_logs/ directory on the local host

```
- POSTCHECK OPTION
```
Run the postcheck option after the window by specifying the following parameters
    --tags postcheck
    -e "variable_host=<inventory_host/group_name>"
    -e "cmds_file=<cli_cmds_file.txt>" 

- Example: ansible-playbook sdwan_cli_capture.yml --tags postcheck -e "variable_host=vEdge1" -e "cmds_file=sdwan_cli_cmds.txt"

- All output logs are stored in the /tmp/ansible_logs/ directory on the local host

```
- READY FOR DIFF COMPARISON
```
Once the postcheck collection has completed, run the sdwan_pre_post_diff.yaml playbook to perform the diff comparison.
 ```


## 2. Data comparison - sdwan_pre_post_diff.yml

### SDWAN PRECHECK and POSTCHECK DIFF COMPARISON PLAYBOOK

#### Playbook Requirements:

   1: Playbook requires the python3 script diff_cleanup.py to be located inside the lib dir. 


#### Playbook Summary:

   1: Playbook will take two files as input and perform a diff comparison between them and create 2 diff output files. Script can be used to compare any 2 files.

   2: The sdwan_pre_post_diff.yml playbook requires these 4 input variables to be defined when executed.
```
   a: File Location -> User needs to provide the full Path where both files are located, and oth files need to be in the same location.
         -e "file_location=</full/path/>"

   b: Precheck File Name -> User needs to provide the exact name of the precheck file.
         -e "precheck_file=<precheck_filename>"

   c: Postcheck File Name -> User needs to provide the exact name of the postcheck file.
         -e "postcheck_file=<postcheck_filename>"

   d: Diff Fileid Varibale -> User needs to provide a value or name that can be used as part of the diff output file name. This is just to make it easy to identify the diff output file.
         -e "fileid=<diff_filename>"
```

   3: Example of how to run the sdwan_pre_post_diff.yml playbook.
   
```
   Example: ansible-playbook sdwan_pre_post_diff.yml -e "file_location=/tmp/ansible_logs/" -e "precheck_file=vEdge_Precheck.txt" -e "postcheck_file=vEdge_Postcheck.txt" -e "fileid=Full_collection" 
```
   a: Script Produces 2 HTML Output Files
         1: Produces an Output file called - {{filename}}_diff_{{timestamp}}.html
            This file contains the Full Precehck - Postcheck diff comparisons side by side.

         2: Produces an Output file called - {{filename}}_reduced_diff_{{timestamp}}.html
            This file contains the reduced line by line diff comparison. -->

# Installation
*Provide information on how to install the package. e.g.*

1. git clone project using `git clone https://wwwin-github.cisco.com/CX-TTG-SRE-LAB/SDWAN-CLI-CAPTURE.git`
2. follow instructions listed above to get started using the scripts
